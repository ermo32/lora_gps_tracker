# LoRa GPS tracker

This GPS tracker can be used as an object tracking device. For instance cars, bicycles, etc. can be tracked in case of theft. Also animals, humans can be tracked to monitor their locations.

Next specifications should be included in the design:
- Broad support of supply voltages (10 upto 42 Volts)
- Small form factor to enable ease of mounting
- Low power to extend battery life time.
- Open source
- Open source development tools (KiCAD, FeeCAD, etc.)
